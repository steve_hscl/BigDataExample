package com.centrica.bigdataexample;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.ToolRunner;
import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;

/**
 * Created by Steve Harrison on 09/11/17.
 */
public class ProcessPostCodesTest
{
    private static final ClassLoader    CLASS_LOADER    =   ProcessPostCodesTest.class.getClassLoader();
    private static final Path           TEST_BASE_DIR;

    static
    {
        try
        {
            TEST_BASE_DIR = Paths.get(CLASS_LOADER.getResource("").toURI());
        }
        catch (URISyntaxException e)
        {
            // Not sure under what circumstances this can possibly happen but as the code won't compile without it...
            throw
                new RuntimeException("Failed to get valid URI from " + CLASS_LOADER.getResource("").toString());
        }
    }

    @Test
    public void testMRJob()
    throws
        Exception
    {
        assertEquals(
            "Failed to run MR Job successfully",
            ToolRunner.run(
                new Configuration(),
                new ProcessPostCodes(),
                new String[]
                    {
                        TEST_BASE_DIR.resolve("Input").toString(),
                        TEST_BASE_DIR.resolve("Output").toString()
                    }),
            0
            );
    }
}