package com.centrica.bigdataexample;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.util.StringTokenizer;
import java.util.stream.StreamSupport;

/**
 * Created by Steve Harrison on 20/11/17.
 */
public class WordCount
{
    public static class TokenizerMapper
    extends
        Mapper<Object, Text, Text, IntWritable>
    {
        private final IntWritable   one     =   new IntWritable(1);
        private final Text          word    =   new Text();

        public void map(Object key, Text value, Context context)
        throws
            IOException,
            InterruptedException
        {
            StringTokenizer tokenizedValue = new StringTokenizer(value.toString());

            while (tokenizedValue.hasMoreTokens())
            {
                word.set(tokenizedValue.nextToken());
                context.write(word, one);
            }
        }
    }

    public static class IntSumReducer
    extends
        Reducer<Text,IntWritable,Text,IntWritable>
    {
        private final IntWritable result = new IntWritable();

        public void reduce(Text                     key,
                           Iterable<IntWritable>    values,
                           Context                  context)
        throws
            IOException,
            InterruptedException
        {
            result
                .set(
                    StreamSupport.stream(values.spliterator(), false)
                    .mapToInt(IntWritable::get)
                    .sum());

            context.write(key, result);
        }
    }

    public static void main(String[] args)
    throws
        Exception
    {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "Word Count");
        job.setJarByClass(WordCount.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
