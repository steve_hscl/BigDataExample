package com.centrica.bigdataexample;

import org.apache.commons.lang.ArrayUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by Steve Harrison on 09/11/17.
 */
public class ProcessPostCodes
extends
    Configured
implements
    Tool
{
    private static final Logger LOGGER  = Logger.getLogger(ProcessPostCodes.class);
    private static final int    INPUT_PATH  =   0,
                                OUTPUT_PATH =   1,
                                ALL_OKAY    =   0,
                                FAILED      =   1,
                                USAGE_ERROR =   2;

    private Configuration   configuration;

    private void usage()
    {
        LOGGER.error("Usage Error:");
        LOGGER.error("ProcessPostCodes <inputDir> <outputDir>");
    }

    /**
     * Checks to see if the outputPath exists. If it does it is removed otherwise the MR Job will fail
     * @param outputPath path to be checked
     */
    private void checkOutputPath(Path outputPath)
    {
        FileSystem fileSystem;

        try
        {
            fileSystem = outputPath.getFileSystem(configuration);

            if (fileSystem.exists(outputPath))
            {
                fileSystem.delete(outputPath, true);
            }
        }
        catch (IOException e)
        {
            throw
                new RuntimeException("Failed to check or delete output path", e);
        }
    }

    @Override
    public int run(String[] args)
    throws
        Exception
    {
        GenericOptionsParser    optionsParser;
        Path                    inputPath,
                                outputPath;
        String[]                paths;
        Job                     job;

        LOGGER.info("Arguments: " + ArrayUtils.toString(args));

        // First set up the environment. Used to be done in the constructor but moved here to
        // implement the Tool interface.
        configuration   =   getConf();

        try
        {
            optionsParser   =   new GenericOptionsParser(configuration, args);
        }
        catch (IOException e)
        {
            throw
                new RuntimeException("Failed to parse options", e);
        }

        paths   =   optionsParser.getRemainingArgs();

        if (paths.length < 2)
        {
            usage();

            return USAGE_ERROR;
        }
        else
        {
            inputPath   =   new Path(paths[INPUT_PATH]);
            outputPath  =   new Path(paths[OUTPUT_PATH]);

            if (paths.length > 2)
            {
                LOGGER.warn(
                    "Ignoring additional parameters: " +
                        Arrays
                            .stream(paths, 2, paths.length - 1)
                            .collect(Collectors.joining(" ")));
            }
        }

        checkOutputPath(outputPath);

        // Now set up the job
        job =   Job.getInstance(configuration, "Process Postcodes");

        job.setInputFormatClass(TextInputFormat.class);
        TextInputFormat.addInputPath(job, inputPath);
        job.setOutputFormatClass(TextOutputFormat.class);
        TextOutputFormat.setOutputPath(job, outputPath);
        job.setMapperClass(PostCodeMapper.class);
        job.setNumReduceTasks(1);
        job.setReducerClass(PostCodeReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        if (job.waitForCompletion(true))
        {
            return ALL_OKAY;
        }
        else
        {
            return FAILED;
        }
    }

    public static void main(String[] args)
    throws
        Exception
    {
        System.exit(ToolRunner.run(new Configuration(), new ProcessPostCodes(), args));
    }
}
