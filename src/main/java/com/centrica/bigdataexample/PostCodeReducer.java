package com.centrica.bigdataexample;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

/**
 * Created by Steve Harrison on 09/11/17.
 */
public class PostCodeReducer
    extends
    Reducer<Text, Text, Text, Text>
{
    private static final Logger LOGGER  =   Logger.getLogger(PostCodeReducer.class);

    private final Text  outputValue =   new Text();

    public void reduce(Text             key,
                       Iterable<Text>   values,
                       Context          context)
    throws
        IOException,
        InterruptedException
    {
        StreamSupport
            .stream(values.spliterator(), false)
            .forEach(
                value ->
                {
                    // Do something here to accumulate the required result.
                    // Can also increments some counters if you want.
                });

        context.write(key, outputValue);
    }
}
