package com.centrica.bigdataexample;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Steve Harrison on 09/11/17.
 */
public class PostCodeMapper
extends
    Mapper<WritableComparable, Text, Text, Text>
{
    static final String             FIELD_DELIMITER         =   "|",
                                    AREA_ID                 =   "AREA",
                                    DISTRICT_ID             =   "DISTRICT",
                                    SECTOR_ID               =   "SECTOR",
                                    POSTCODE_ID             =   "FULL",
                                    LARGE_USER_DISTRICT_ID  =   "GIR";
    static final Pattern            FIELD_SPLIT             =   Pattern.compile(Pattern.quote(FIELD_DELIMITER)),
                                    POST_CODE_CHECK         =
                                        Pattern.compile(
                                            String.format(
                                                "^(?<%s>" +
                                                    "(?<%s>%s|" +
                                                    "(?<%s>[A-Z]{1,2})" +
                                                    "\\d{1,2}[A-Z]{0,1}) +" +
                                                    "\\s{0,}\\d)" +
                                                    "[A-Z]{2}$",
                                                SECTOR_ID,
                                                DISTRICT_ID,
                                                LARGE_USER_DISTRICT_ID,
                                                AREA_ID));

    private static final Logger     LOGGER                  =   Logger.getLogger(PostCodeMapper.class);

    private enum Field
    {
        POSTCODE,
        ACTIVE,
        LATITUDE,
        LONGITUDE,
        EASTING,
        NORTHING,
        GRID_REF,
        COUNTY,
        DISTRICT,
        WARD,
        DISTRICT_CODE,
        WARD_CODE,
        COUNTRY,
        COUNTY_CODE,
        CONSTITUENCY,
        INTRODUCED,
        TERMINATED,
        PARISH,
        NATIONAL_PARK,
        POPULATION,
        HOUSEHOLDS,
        BUILT_UP_AREA,
        BUILT_UP_SUB_DIVISION,
        LOWER_LAYER_SUPER_OUTPUT_AREA,
        RURAL_OR_URBAN,
        REGION,
        ALTITUDE,
        LONDON_ZONE,
        LSOA_CODE,
        LOCAL_AUTHORITY,
        MSOA_CODE,
        MIDDLE_LAYER_SUPER_OUTPUT_AREA,
        PARISH_CODE,
        CENSUS_OUTPUT_AREA;

        private static final
        HashMap<
            Integer,
            Field>      FIELD_BY_FIELD_INDEX;

        static
        {
            FIELD_BY_FIELD_INDEX    =   new HashMap<>();

            Arrays
                .stream(Field.values())
                .forEach(field -> FIELD_BY_FIELD_INDEX.put(field.ordinal(), field));
        }

        private static Field getFieldByIndex(int fieldIndex)
        {
            return FIELD_BY_FIELD_INDEX.get(fieldIndex);
        }

        private Field getNext()
        {
            return FIELD_BY_FIELD_INDEX.get(ordinal() + 1);
        }
    }

    private enum PostCodeStatus
    {
        INVALID_POSTCODE,
        INACTIVE_POSTCODE,
        IGNORED_POSTCODE,
        ACTIVE_POSTCODE,
        INVALID_FIELDS,
        HEADER
    }

    private final Text  outputKey   =   new Text(),
                        outputValue =   new Text();

    private String          postcode,
                            districtId;
    private String[]        rawFields;
    private Matcher         postcodeMatcher;

    private PostCodeStatus validatePostcode()
    {
        PostCodeStatus  postCodeStatus;

        postcode        =   rawFields[Field.POSTCODE.ordinal()].trim();
        postCodeStatus  =   PostCodeStatus.ACTIVE_POSTCODE;
        postcodeMatcher =   POST_CODE_CHECK.matcher(postcode);

        // First check that the postcode is a valid postcode
        if (postcodeMatcher.matches())
        {
            districtId  =   postcodeMatcher.group(DISTRICT_ID);

            if (districtId.equals(LARGE_USER_DISTRICT_ID))
            {
                postCodeStatus  =   PostCodeStatus.IGNORED_POSTCODE;
            }
        }
        else
        {
            // Check to see if the invalid is the header. Skip it if it is
            if ("postcode".equalsIgnoreCase(postcode))
            {
                return PostCodeStatus.HEADER;                           //****EARLY RETURN***/
            }

            postCodeStatus  =   PostCodeStatus.INVALID_POSTCODE;
        }

        // Check that the postcode is Active
        switch (rawFields[Field.ACTIVE.ordinal()].trim().toLowerCase())
        {
            case "yes":
                if (postCodeStatus == PostCodeStatus.INVALID_POSTCODE)
                {
                    LOGGER.debug("Invalid postcode found " + postcode);
                }
                // No other action is required.
                break;

            case "no":
                // Set the postcode to be inactive. Can overwrite if invalid as the postcode is inactive
                postCodeStatus  =   PostCodeStatus.INACTIVE_POSTCODE;
                break;

            default:
                // The active field does not contain what is expected
                postCodeStatus  =   PostCodeStatus.INVALID_FIELDS;
        }

        return postCodeStatus;
    }

    @Override
    public void map(WritableComparable  key,
                    Text                value,
                    Context             context)
    throws
        IOException,
        InterruptedException
    {
        PostCodeStatus  postCodeStatus;

        rawFields       =   FIELD_SPLIT.split(value.toString());

        postCodeStatus  =   validatePostcode();

        // Now increment the counter to show totals
        context.getCounter(postCodeStatus).increment(1L);

        //... Add some code to set the outputKey and outputValue based on requirements
        //... outputKey.set(key required)
        //... outputValue.set(value required)

        context.write(outputKey, outputValue);
    }
}
