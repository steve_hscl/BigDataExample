# Big Data Example

The code in this project is designed as an example of how to use
MapReduce on a Hadoop platform. The task is to produce similar
output to that shown [here](https://www.doogal.co.uk/UKPostcodes.php).
The only bits of the report required are

1. Postcode area
2. Population
3. Households
4. Postcodes
5. Active postcodes.

Please ignore Area covered and Non-geographic postcodes as these
fields are not actually available in the data
(well as far as I can work out).

The data has been converted into "|" delimited from the CSV provided by
[website](https://www.doogal.co.uk/UKPostcodes.php).
This is mainly because the data contains commas and
dealing with enclosed fields is a pain, i.e. can't use just a regular
expression to split the fields.
The data is available in the test resources in the Input directory.
Note: It is stored in a gzip file but does not need to be unzipped
before use as Hadoop copes with this. It's also worth noting that the
input file is too large to be opened by Excel. It contains over 2.5 million
postcodes. Excel limit is currently 1,048,576 or 2<sup>20</sup> and therefore
about 1 million rows short.

The driver class for this example is **ProcessPostCode**. You should
not need to change this class, unless you really want to.

The Mapper class (**PostCodeMapper**) contains a regular expression to help
decode the post codes (assuming they are valid) into the various parts. This
regular expression uses named capture groups for the various parts of the
postcode. It also contains an enumeration of the fields contained in the raw data
file and the initial processing of the postcode.

The Reducer class (**PostCodeReducer**) contains the bare bones. I've used
a stream to process as a starter but this can be replaced with a for loop
if desired.

A single test case is provided (**ProcessPostCodesTest**). Running this
Unit Test should spin up a Hadoop Mini Cluster and execute your MapReduce
code. The directory Output which should appear in target under test-classes.

The **WordCount** class is a self contained word count class. No unit
test has yet been provided for this class as it is simply for documentation.