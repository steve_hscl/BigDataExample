# Hadoop on Windows

Getting the sample code working on Windows can be a bit of a mission.
I've always run under Linux and only recently tried to run on a
Windows platform. To even run the MiniCluster for testing purposes you
will need some of the Hadoop binaries. I tried to install these properly
from the Hortonworks website but the installer always seem fail, so I
extracted all of the salient information from the MSI and here is an
easyish guide to making Hadoop work, for your unit testing at least.

1. First get hold of this [file](https://www.dropbox.com/l/scl/AADa5SFUcceM8XEGqlxQpFUP2SNAYMISHiw).
2. Unpacked the contents into a directory. I used C:\Hadoop on my PC, but it can go anywhere.
3. Edit your system variables and add the setting HADOOP_HOME and set it to the directory used above.
4. Edit your Path and add %HADOOP_HOME%/bin to the end.
5. Windows being windows it is probably best to reboot at this point.

Good Luck


